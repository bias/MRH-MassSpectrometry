from distutils.core import setup

setup(
    name='MRH',
    version='1.0',
    packages=['MRH'],
    url='https://gitlab.ics.muni.cz/bias/MRH-MassSpectrometry.git',
    license='MIT',
    author='barton',
    author_email='barton@vutbr.cz',
    description='MRH - Multiresolution Hierarchy for Mass Spectrometry'
)
