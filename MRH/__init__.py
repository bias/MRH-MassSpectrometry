import pandas as pd
import numpy as np
import scipy.stats as sps
import skimage.measure as mesr
import skimage.morphology as morph
import skimage.segmentation as segm
import scipy.interpolate as sinterp
import scipy.ndimage as ndi

import matplotlib.pyplot as plt
import multiprocessing
import cv2
import os
import pymzml
import treelib

from joblib import Parallel, delayed
from itertools import chain

import warnings
warnings.filterwarnings("ignore")


#
# MZML_DATA_PROCESSING
#
def mzml_data_processing(mzml_file):
    """Converting mzML file to pandas dataframe.

    Params:
        mzml_file: path to the mzML file.

    Returns:
        a pandas dataframe with measured datapoints

    """
    run = pymzml.run.Reader(mzml_file)
    spectra = pd.DataFrame(
        columns=["index", "ms_level", "rt", "mz", "intensity"])
    spectrum = pd.DataFrame(
        columns=["index", "ms_level", "rt", "mz", "intensity"])
    for _ in range(0, run.get_spectrum_count()):
        spc = run.next()
        spectrum["intensity"] = spc.i
        spectrum["mz"] = spc.mz
        spectrum["rt"] = spc.scan_time_in_minutes() / 60
        spectrum["ms_level"] = spc.ms_level
        spectrum["index"] = spc.index

        spectra = spectra.append(spectrum)
        spectrum = spectrum.drop(spectrum.index)

    return spectra


#
# ZERO_INTENSITY_REMOVAL
#
def zero_intensity_removal(spectra: pd.DataFrame,
                           in_situ=True) -> pd.DataFrame:
    """Removing zero intensity points from spectras to process.

    Params:
        spectra: a Pandas data frame with columns 'rt', 'mz' and 'intensity'
        in_situ (boolean): should all filtering happen in provided data frame,
            or a new data frame should be created?

    Returns:
        a Pandas data frame with zero intensity points removed
    """
    if in_situ:
        X = spectra[spectra.intensity != 0]
    else:
        X = spectra[spectra.intensity != 0].copy()

    return X


#
# NOISE_FILTERING_SIMPLE
#
def noise_filtering_simple(spectra: pd.DataFrame,
                           noise_model='constant_quantile',
                           in_situ=True,
                           **kwargs) -> pd.DataFrame:
    """Implements various simple strategies for noise filtering.

    Params:
        spectra: a Pandas data frame with columns 'rt', 'mz' and 'intensity'
        noise_model:
            'constant_quantile': the noise level is taken as the value of the
                'q' quantile of the intensity values
            'constant': the noise level is fixed to 'threshold'
        in_situ (boolean): should all filtering happen in provided data frame,
            or a new data frame should be created?
        Variable arguments:
            q: float (0,1) for quantile estimation of the noise
            threshold: float, fixed noise level (all values above are considered
                true signal)

    Returns:
        a Pandas data frame with noise removed

    Example:
        noise_filtering_simple(S, noise_model='constant', threshold=1.0)
    """
    noise_model = noise_model.lower()
    noise_model_list = ['constant', 'constant_quantile']
    if noise_model not in noise_model_list:
        raise RuntimeError('Unknown noise model: ' + noise_model)

    if in_situ:
        X = spectra
    else:
        X = spectra.copy()

    if noise_model == 'constant':
        threshold = kwargs['threshold']
        idx = X['intensity'] > threshold
        X = X[idx]

        return X

    if noise_model == 'constant_quantile':
        threshold = X['intensity'].quantile(q=kwargs['q'])
        idx = X['intensity'] > threshold
        X = X[idx]

        return X


#
# _RESOL10 - change resolution in powers of 10 increments
#
def _resol10(x: np.array, res_pow10: int):
    """Simple change in resolution using a power of 10 scale.
    Each value is rescaled by 10**res_pow10 and rounded to the nearest integer.

    Params:
        x (numpy.array)
        res_pow10 (int)

    Returns
        numpy.array
    """

    z = np.round(x.astype(np.float64) * 10 ** res_pow10).astype(np.int64)

    return z


#
# GRIDDING
#
class Gridder(object):
    """A simple class to capture the data gridding properties."""

    def __init__(self,
                 rp10_mz: int,
                 rp10_rt: int,
                 mz_min: int,
                 rt_min: int,
                 mz_max: int,
                 rt_max: int,
                 binning_step: int = 1):

        # resolution
        self._res_pow10_mz = rp10_mz
        self._res_pow10_rt = rp10_rt

        # get the bounding box in desired resolution coords
        mz_min, mz_max = _resol10(np.array([mz_min, mz_max]),
                                  self._res_pow10_mz)
        rt_min, rt_max = _resol10(np.array([rt_min, rt_max]),
                                  self._res_pow10_rt)

        # define a unique grid for all gridding calls
        self._binning_step = binning_step
        self._rt_bins = np.arange(rt_min - 0.5, rt_max + 1, self._binning_step)
        self._mz_bins = np.arange(mz_min - 0.5, mz_max + 1, self._binning_step)

        # if self._rt_bins.shape[0] == 0 or self._mz_bins.shape[0] == 0:
        # raise RuntimeError('Not enough bins, modify min/max for rt/mz')

        return

    @property
    def res_pow10_mz(self):
        return self._res_pow10_mz

    @property
    def res_pow10_rt(self):
        return self._res_pow10_rt

    @property
    def rt_min(self):
        return self._rt_bins.min()

    @property
    def rt_max(self):
        return self._rt_bins.max()

    @property
    def mz_min(self):
        return self._mz_bins.min()

    @property
    def mz_max(self):
        return self._mz_bins.max()

    @property
    def mz_grid(self):
        return self._mz_bins

    @property
    def rt_grid(self):
        return self._rt_bins

    @property
    def binning_step(self):
        return self._binning_step

    def reset(self, rp10_mz: int, rp10_rt: int, mz_grid: np.array,
              rt_grid: np.array, binning_step: int):
        self._res_pow10_mz = rp10_mz
        self._res_pow10_rt = rp10_rt
        self._binning_step = binning_step
        self._rt_bins = rt_grid
        self._mz_bins = mz_grid
        return

    def summary(self,
                spectra: pd.DataFrame,
                aggregator='max',
                cell_zeroing=0.0) -> np.ndarray:
        """Projects a set of spectra onto a regular grid of (mz, rt) measurements and
        computes a summary for the data. For this, it multiplies the 'mz' and 'rt'
        with 10**res_pow10, rounds the resulting values to the nearest integer and
        summarizes the intensities falling to the same (mz, rt) coordinates using the
        specified aggregator function.

        Params:
            spectra: a Pandas data frame with columns 'rt', 'mz' and 'intensity'
            aggregator: 'hist', 'count', 'mean', 'median', 'sum', 'max', 'min'.
                Indicates the method for aggregating the intensities falling onto the
                same (mz, rt) coordinates. See scipy.stats.binned_statistic_2d(). Note:
                'hist' and 'count' are identical.
            cell_zeroing: (float) all cells (after gridding) with values below this parameter
                will be set to 0

        Returns:
            numpy.ndarray an array with the summarized signal
        """
        aggregator = aggregator.lower()
        agg_list = [
            'hist', 'median', 'min', 'max', 'count', 'mean', 'sum', 'std'
        ]
        if aggregator not in agg_list:
            raise RuntimeError('Unknown aggregator {}'.format(aggregator))

        rt = _resol10(spectra['rt'].values, self._res_pow10_rt)
        mz = _resol10(spectra['mz'].values, self._res_pow10_mz)

        if aggregator == 'hist':
            aggregator = 'count'

        try:
            res = sps.binned_statistic_2d(mz,
                                          rt,
                                          spectra['intensity'],
                                          statistic=aggregator,
                                          bins=[self._mz_bins, self._rt_bins])
        except MemoryError:
            raise RuntimeError('Out of memory - reduce grid size')

        res.statistic[np.isnan(res.statistic)] = 0.0
        if cell_zeroing > 0:
            res.statistic[res.statistic <= cell_zeroing] = 0.0

        return res.statistic

    def map(self, data) -> tuple:
        """Maps a set of measurements onto the grid and returns the (i,j)
        coordinates for each point (row) in data. The data is provided in the
        original space and the resolution is changed according to the params
        used for building the grid.

        Params:
            data: -if numpy.array, it is considered to have n rows and at least
                two columns (the first two are used). First column would correspond
                to "mz" values and the second one to "rt" values.
                  -if pandas.DataFrame, then the columns of interest should be
                named "mz" and "rt"

        Returns:
            a tuple (i, j) with row and column coordinates in the grid. The grid
            is (0,0)-based indexed.
            For the values that are outside the grid, the corresponding i and j
            values are set to -1 (since they are indexes, negative values will
            indicate an exceptional case).
        """

        if isinstance(data, np.ndarray):
            i = np.digitize(_resol10(data[0], self._res_pow10_mz),
                            self._mz_bins,
                            right=False)
            j = np.digitize(_resol10(data[1], self._res_pow10_rt),
                            self._rt_bins,
                            right=False)
        elif isinstance(data, pd.DataFrame):
            i = np.digitize(_resol10(data['mz'].values, self._res_pow10_mz),
                            self._mz_bins,
                            right=False)
            j = np.digitize(_resol10(data['rt'].values, self._res_pow10_rt),
                            self._rt_bins,
                            right=False)
        else:
            raise RuntimeError("data of unknown type")

        # set values outside the grid to 0, initially - since we will subtract 1
        # everything outside becomes -1...
        # i[i == 0] = -1 # not needed
        # j[j == 0] = -1 # not needed
        i[i == self._mz_bins.size] = 0
        j[j == self._rt_bins.size] = 0

        # get the indexes to be 0-based (and those outside become -1)
        i -= 1
        j -= 1

        return i, j


#
# EXTRACT_ROIS
#
def _extract_roi_worker(spectra: pd.DataFrame, grid: Gridder, pr,
                        roi_root_path, keep_data) -> dict:
    roi = {
        'roi_path':
            (roi_root_path + '/' + str(pr.label)),  # use the region label as index
        'grid': {
            'res_pow10_mz': grid.res_pow10_mz,
            'res_pow10_rt': grid.res_pow10_rt,
            'mz_bins': grid.mz_grid,
            'rt_bins': grid.rt_grid,
            'binning_step': grid.binning_step
        },
        'bbox': pr.bbox,  # this is in "grid" coords, i.e. 0,1,...
        'mask': pr.image,  # mask over grid
        'area': pr.area,  # total count of grid cells with data
        'data': None,
        'features': {
            'eccentricity': pr.eccentricity,
            'max_intensity': pr.max_intensity,
            'solidity': pr.solidity,
            'weighted_local_centroid': pr.weighted_local_centroid,
            'convex_area': pr.convex_area
        }
    }
    if keep_data:
        # select measurements within the bounding box
        k_mz, k_rt = grid.map(
            spectra)  # returns (k_mz, k_rt) coords in the grid

        i_mz, = np.where((k_mz >= pr.bbox[0]) & (k_mz < pr.bbox[2]))
        i_rt, = np.where((k_rt >= pr.bbox[1]) & (k_rt < pr.bbox[3]))

        i = np.intersect1d(i_mz, i_rt, assume_unique=True)

        # keep only those measurements that are within
        # the mask for the region: need to convert to bounding box coords:
        k, = np.where(pr.image[k_mz[i] - pr.bbox[0], k_rt[i] - pr.bbox[1]])

        # get the data frame
        roi['data'] = spectra.iloc[i[k]]
        # roi['data'] = i[k]

    return roi


def extract_ROIs(spectra: pd.DataFrame,
                 res_pow10_mz: float,
                 res_pow10_rt: float,
                 bounding_box: tuple = None,
                 min_gap=1,
                 roi_root_path='',
                 keep_data=True):
    """Extract regions of interest (ROIs) from 2D (M/Z, Rt) data array.
    A ROI is defined as a set of neighboring points where the intensity of the
    signal is non-zero.

    Params:
        spectra: a Pandas data frame with columns 'rt', 'mz' and 'intensity'
        res_pow10_{mz,rt}: (float) the data is multiplied by 10^res_pow10 before
            gridding. It means that for res_pow10 > 0 the effect is of 'zooming in',
            while for res_pow10 < 0 the effect is 'zooming out'. Must be exactly
            the same as those used for producing X (by gridding()).
        bounding_box: ([mz_min, rt_min,] mz_max, rt_max) specifies the domain of
            mz/rt space to operate on. All values are given in original coordinates
            (as "spectra" values). If the tuple has only two values, they are
            considered to specify the maximum mz and rt, respectively, with minimum
            defaulting to 0.
        min_gap (int): minimum distance between regions to be considered separate (in
            integer units)
        roi_root_path (string): path to the current root in the multiresolution tree
        keep_data (bool): whether to save data for each ROI in the returned list

    Return:
        a list of ROIs, i.e. dictionaries with the following fields:
            'roi_path': each ROI has a unique path
            'res_pow10_mz': res_factor_mz
            'res_pow10_rt': res_factor_rt
            'bbox': bounding box
            'mask': pr.image,
            'area': pr.area,
            'data': None
    """
    if bounding_box is None:
        mz_min = spectra['mz'].min()
        rt_min = spectra['rt'].min()
        mz_max = spectra['mz'].max()
        rt_max = spectra['rt'].max()
    else:
        if len(bounding_box) == 2:
            mz_min, rt_min = 0, 0
            mz_max, rt_max = bounding_box
        else:
            mz_min, rt_min, mz_max, rt_max = bounding_box

    # build the grid
    grid = Gridder(res_pow10_mz,
                   res_pow10_rt,
                   mz_min,
                   rt_min,
                   mz_max,
                   rt_max,
                   binning_step=1)
    X = grid.summary(spectra, aggregator='count')

    M = (X > 0).astype(np.uint8)  # binary image

    # 1. Fill in the small gaps between parts
    if min_gap > 1:
        # maybe some other "gap filling" from mahotas pkg would be better
        M = morph.binary_dilation(M, selem=morph.disk(min_gap))
        M = morph.binary_closing(M, selem=morph.disk(min_gap))

    # 2. Find connected components:
    regs, nregs = morph.label(M, connectivity=2, return_num=True)
    if nregs == 0:
        return []
    regs, _, _ = segm.relabel_sequential(regs)

    props = mesr.regionprops(regs, X)
    props = sorted(props, key=lambda _p: _p.bbox[0])

    print("{} ROIs detected".format(nregs))
    n_cores = multiprocessing.cpu_count()

    # ROIs = Parallel(n_jobs=n_cores, temp_folder = '/dev/shm', backend='multiprocessing')(delayed(_extract_roi_worker)(spectra, grid, pr, roi_root_path, keep_data) for pr in props)
    ROIs = Parallel(n_jobs=n_cores, temp_folder='/dev/shm')(delayed(
        _extract_roi_worker)(spectra, grid, pr, roi_root_path, keep_data)
                                                            for pr in props)

    return ROIs


def create_roi_feature_dict(roi):
    """

    """
    features = {
        'roi_path': roi['roi_path'],
        'grid': {
            'res_pow10_mz':
                int(roi['grid']['res_pow10_mz']),
            'res_pow10_rt':
                int(roi['grid']['res_pow10_rt']),
            'mz_bins': (float(roi['grid']['mz_bins'].min()) /
                        (10 ** int(roi['grid']['res_pow10_mz'])),
                        float(roi['grid']['mz_bins'].max()) /
                        (10 ** int(roi['grid']['res_pow10_mz']))),
            'rt_bins': (float(roi['grid']['rt_bins'].min()) /
                        (10 ** int(roi['grid']['res_pow10_rt'])),
                        float(roi['grid']['rt_bins'].max()) /
                        (10 ** int(roi['grid']['res_pow10_rt']))),
            'binning_step':
                float(roi['grid']['binning_step'])
        },
        'bbox': roi['bbox'],
        'features': {
            'area':
                float(roi['area']),
            'PointsCount':
                int(len(roi['data'].index)),
            'eccentricity':
                float(roi['features']['eccentricity']),
            'max_intensity':
                float(roi['features']['max_intensity']),
            'solidity':
                float(roi['features']['solidity']),
            'weighted_local_centroid':
                (float(roi['features']['weighted_local_centroid'][0]),
                 float(roi['features']['weighted_local_centroid'][1])),
            'convex_area':
                float(roi['features']['convex_area'])
        }
    }

    return features


class Region:
    """

    """

    def __init__(self, data=None, mask=None, features=None, peaks=None, residues_data=None):
        if data is None:
            data = []
        self.data = data
        self.residues_data = residues_data
        self.mask = mask
        self.features = features
        self.peaks = peaks
        self.expanded = False


def create_tree(spectra, res_mz, res_rt, min_gap, min_roi_size=50):
    tree = treelib.Tree()
    tree.create_node('root', data=Region(data=spectra))

    for i in np.arange(0, len(res_mz)):
        print(f'decomposition of level {str(i)}')
        leaves = tree.leaves()
        for leaf in leaves:
            if not leaf.data.expanded:
                roi_data = leaf.data
                rois = extract_ROIs(spectra=roi_data.data,
                                res_pow10_mz=res_mz[i],
                                res_pow10_rt=res_rt[i],
                                min_gap=min_gap[i],
                                roi_root_path=leaf.identifier)

                for roi in rois:
                    if (len(roi['data'].index) >= min_roi_size) & (sps.variation(roi['data']['rt']) > 0):
                        node_data = Region(data=roi['data'], mask=roi['mask'], features=create_roi_feature_dict(roi))
                        tree.create_node(parent=leaf.identifier, data=node_data)
                        leaf.data.data.drop(roi['data'].index, inplace=True)

                leaf.data.residues_data = leaf.data.data.copy()
                leaf.data.data = leaf.data.data.iloc[0:0]
                leaf.data.expanded = True

    return tree


#
# PEAK PICKING PART
#
def interpolate_grid(Z, method='linear') -> np.ndarray:
    """Return interpolated image for watershed segmentation.

    Params:
        Z: Array from summary function of Gridder class

    Returns:
        grid: np.ndarray (image) of interpolated data
    """
    indices = np.where(Z > 0)
    val = Z[indices]

    grid = sinterp.griddata(indices,
                            val,
                            np.indices(Z.shape).T,
                            method=method,
                            fill_value=0)

    return grid.T


def watershed_v1(Z):
    """

    Params:
        Z: image to be processed

    Returns:

    """
    mask = (Z > 0).astype(np.uint8)
    distance = Z
    local_maxi = morph.h_maxima(distance, 5000000)
    markers = ndi.label(local_maxi)[0]
    labels = morph.watershed(-distance, markers, mask=mask)

    peaks = []
    lab_ind = []

    for i in np.unique(labels):
        if i != 0:
            mask0 = labels == i
            peaks_ind = np.unravel_index(np.argmax(Z * mask0, axis=None),
                                         Z.shape)
            peaks.append((peaks_ind[0], peaks_ind[1]))
            lab_ind.append(i)

    return labels, peaks, lab_ind


def score(x0, roi, d=(0.001, 5)):

    N0 = roi.loc[roi['intensity'] > float(x0['intensity'] * 0.9)]
    N1 = N0.loc[(N0['mz'] > float((x0['mz']) - d[0])) & (N0['mz'] < float((x0['mz']) + d[0]))]
    N1 = N1.loc[(N0['rt'] > float((x0['rt']) - d[1])) & (N0['rt'] < float((x0['rt']) + d[1]))]

    Q = len(N1) / len(N0)
    # Q = N1['intensity'].sum() / N0['intensity'].sum()

    return Q


def watershed_v2(X, mz_lv, rt_lv):
    """

    """
    peaks = []
    peaksQ = []
    X0 = X.copy()
    grid = Gridder(mz_lv, rt_lv, X['mz'].min(), X['rt'].min(), X['mz'].max(),
                   X['rt'].max())
    img = grid.summary(X, aggregator='max')
    csv = pd.DataFrame(columns=['mz', 'rt', 'intensity'])
    csv_raw = pd.DataFrame(columns=['mz', 'rt', 'intensity'])
    # img2 = ndi.filters.median_filter(img, size=2, mode='constant')
    img2 = img

    if np.count_nonzero(img2) > 50:
        img3 = interpolate_grid(img2, 'linear')
        img4 = cv2.blur(img3, (5, 5), borderType=0)
        # img4[img3 == 0] = 0

        labels, peaks, lab_ind = watershed_v1(img4)

        for p, i in zip(peaks, lab_ind):
            a = p[0]
            b = p[1]
            row = pd.DataFrame([[grid.mz_grid[a] * (10**(-mz_lv)),
                                 grid.rt_grid[b] * (10**-rt_lv), img3[a, b] ]],
                               columns=['mz', 'rt', 'intensity'])

            csv = csv.append(row)

        if csv.empty:
            ind = X['intensity'].idxmax()
            csv = pd.DataFrame(
                [[X['mz'][ind], X['rt'][ind], X['intensity'][ind]]],
                columns=['mz', 'rt', 'intensity'])

        for _, row in csv.iterrows():
            d = np.sqrt(((X0['rt'] - row['rt'] )**2) +
                        ((X0['mz'] - row['mz'] )**2))
            predicted_peak = X0[X0.index == d.idxmin()]
            csv_raw = csv_raw.append(predicted_peak)
            peaksQ.append(score(predicted_peak, X0))

    return csv, csv_raw, peaks, peaksQ


def peak_picker(tree):
    csv = pd.DataFrame(columns=['mz', 'rt', 'intensity'])
    csv_raw = pd.DataFrame(columns=['mz', 'rt', 'intensity'])
    peaks = []
    peaksq = list()

    leaves = tree.leaves()
    for leaf in leaves:
        if not leaf.data.expanded:
            csv0, csv_raw0, peaks0, peaksq0 = watershed_v2(leaf.data.data,
                                                           leaf.data.features['grid']['res_pow10_mz'],
                                                           leaf.data.features['grid']['res_pow10_rt'])

            #csv = csv.append(csv0)
            if csv_raw0 is not None:
                csv_raw = csv_raw.append(csv_raw0)
            #peaks = peaks.append(peaks0)

                peaksq.append(peaksq0)

    csv_raw['score'] = list(chain.from_iterable(peaksq))

    return csv_raw





