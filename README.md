# MRH
**M**ulti **R**esolution **H**irerachy approach to process high-resolution mass spectrometry experiments.

It provides:
- initial data filtering and conversion
- data transformation based on different settable resolution levels
- peak picker based on 2D methods
- ambiguity score system for detected peaks
- simple visualizer for a multiresolution structure

Installation
------------

Aplanat is easily installed in the standard python tradition:

    git clone --recursive https://gitlab.ics.muni.cz/bias/MRH-MassSpectrometry.git
    cd MRH-MassSpectrometry
    pip install -r requirements.txt
    python setup.py install

# Publication
MRH software was published and described in journal article [URL].

# Test data
We provided a dataset for testing and support further comparison and developments. Test data are freely available at zenodo: http://doi.org/10.5281/zenodo.4521878
